#pragma once
const char* hitscan[] =
{
    "Off", // 0
    "Low", // 1
    "Medium", // 2
    "High" //3
    //"BAIM"
};

const char* crosshairStyle[] =
{
    "Cross",
    "Circle",
    "Square",
    "Nazi"
};

const char* boxesesp[] =
{
	"Off",
	"Censored",
	"Normal"
};

const char* antiaimyawtrue[] =
{
    "Off",
    "Switch",
    "1$ Fidget-Spinner",
    "50$ Fidget-Spinner",
    "+180",
    "Lowerbody Simple",
	"Lowerbody Update",
	"Side-Right",
	"Stand 180",
	"Stand Flip",
	"Experimental Lowerbody2"
};

const char* skyboxchanger[] =
{
    "Off",
    "cs_baggage_skybox_",
    "sky_csgo_night02",
    "sky_citynight01",
    "vertigo",
    "sky_cs15_daylight01_hdr"
};

const char* fakelags[] =
{
	"None",
	"Normal"
};

const char* edgeyaws[] =
{
	"Off",
	"Sideways",
	"180",
	"Jitter",
	"Swap"
};


/*					if (type == 1) sideways(yaw);
					else if (type == 2) backwards(yaw);
					else if (type == 3) crooked(yaw);
					else if (type == 4) jitter(yaw, m_pcmd);
					else if (type == 5) swap(yaw);*/


const char* antiaimyawfake[] =
{
    "Off",
    "Switch",
	"1$ Fidget-Spinner",
	"50$ Fidget-Spinner",
    "+180",
    "Lowerbody Simple",
	"Lowerbody Update",
	"Experimental Lowerbody",
	"Stand 180",
	"Stand Flip",
	"Experimental Lowerbody2"
	//"Global Jitter"
};

const char* antiaimpitch[] =
{
    "Off",
    "Down",
    "Emotion"
};

const char* pitchcorrect[] = 
{
	"Actual",
	"Down",
	"Up"
};

const char* handd[] =
{
    "Off",
    "NoHands",
    "Wireframe"
};

const char* Namespammers[] =
{
    "Off",
    "Aimware",
    "Interwebz",
    "kapiSware",
    "hitbox",
    "NeverVAC",
    "NameStealer"
};

const char* Chatspammers[] =
{
    "Off",
    "csgocalm.xyz",
    "hitbox",
    "insult",
    "hentai"
};

const char* chamsMode[] =
{
    "Off",
    "Normal",
    "Flat"
};

const char* chams_sceneend[] =
{
	"Off",
	"Color",
	"Material",
	"Gold"
};

const char* thirdpersontypemode[] =
{
	"Real Angles",
	"Fake Angles"
};

const char* edgemode[] =
{
	"Off",
	"Freestanding"
};

const char* lbyyaw[] =
{
	"Off",
	"Normal",
	"Complex"
};

const char* resolvery[] = 
{
	// some extra info for pasters:
	"Off", // nothing 
	"Basic", // simple ayyware
	"Natural-Simple", // biggest meme ever (bruteforce)
	"LBY-Based", // would be hitting p in 2k15 and prob early 2016
	"Reverse Complex", // it was created to reverse "complex" aa's but it's shit
	"frosty.pw", // pasted from frosty 5/10
	"Natural V2" // prob best
};

const char* themes1[] = 
{
	"Dark",
	"Purple",
	"Perfect",
	"Overlay",
	"cantresolve.us"
};

const char* aim_types[] =
{
	"Off",
	"Client-Sided"
};

const char* aimBones[] =
{
    "Head",
    "Neck",
    "Chest",
    "Stomach"
};

const char* configFiles[] =
{
    "Trigger",
    "Rage",
	"Hack vs Hack",
	"Hack vs Hack 2"
};
const char* themes[] =
{
    "RayTeak",
    "Monochrome",
    "Default"

};
const char* colors[] =
{
    "Red",
    "Green",
    "Blue",
    "White",
    "Black"
};


const char* keyNames[] =
{
    "",
    "Mouse 1",
    "Mouse 2",
    "Cancel",
    "Middle Mouse",
    "Mouse 4",
    "Mouse 5",
    "",
    "Backspace",
    "Tab",
    "",
    "",
    "Clear",
    "Enter",
    "",
    "",
    "Shift",
    "Control",
    "Alt",
    "Pause",
    "Caps",
    "",
    "",
    "",
    "",
    "",
    "",
    "Escape",
    "",
    "",
    "",
    "",
    "Space",
    "Page Up",
    "Page Down",
    "End",
    "Home",
    "Left",
    "Up",
    "Right",
    "Down",
    "",
    "",
    "",
    "Print",
    "Insert",
    "Delete",
    "",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "",
    "",
    "",
    "",
    "",
    "Numpad 0",
    "Numpad 1",
    "Numpad 2",
    "Numpad 3",
    "Numpad 4",
    "Numpad 5",
    "Numpad 6",
    "Numpad 7",
    "Numpad 8",
    "Numpad 9",
    "Multiply",
    "Add",
    "",
    "Subtract",
    "Decimal",
    "Divide",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",

};

const char* knives[] =
{
    "Bayonet",
    "Bowie Knife",
    "Butterfly Knife",
    "Falchion Knife",
    "Flip Knife",
    "Gut Knife",
    "Huntsman Knife",
    "Karambit",
    "M9 Bayonet",
    "Shadow Daggers"
};
const char* gloves[] =
{
    "Off",
    "Bloodhound",
    "Sport",
    "Driver",
    "Wraps",
    "Motorcycle",
    "Specialist",
	"Hydra"
};

const char* knifeskins[] =
{
    "None",
    "Crimson Web",
    "Bone Mask",
    "Fade",
    "Night",
    "Blue Steel",
    "Stained",
    "Case Hardened",
    "Slaughter",
    "Safari Mesh",
    "Boreal Forest",
    "Ultraviolet",
    "Urban Masked",
    "Scorched",
    "Rust Coat",
    "Tiger Tooth",
    "Damascus Steel",
    "Damascus Steel",
    "Marble Fade",
    "Rust Coat",
    "Doppler Ruby",
    "Doppler Sapphire",
    "Doppler Blackpearl",
    "Doppler Phase 1",
    "Doppler Phase 2",
    "Doppler Phase 3",
    "Doppler Phase 4",
    "Gamma Doppler Phase 1",
    "Gamma Doppler Phase 2",
    "Gamma Doppler Phase 3",
    "Gamma Doppler Phase 4",
    "Gamma Doppler Emerald",
    "Lore"
};

const char* m4a1s[] =
{
    "Decimator",
    "Knight",
    "Chantico's Fire",
    "Golden Coi",
    "Hyper Beast",
    "Master Piece",
    "Hot Rod",
    "Mecha Industries",
    "Cyrex",
    "Icarus Fell",
    "Flashback",
    "Leaded Glass"
};

const char* ak47[] =
{
    "Fire Serpent",
    "Fuel Injector",
    "Bloodsport",
    "Vulcan",
    "Case Hardened",
    "Hydroponic",
    "Aquamarine Revenge",
    "Frontside Misty",
    "Point Disarray",
    "Neon Revolution",
    "Red Laminate",
    "Redline",
    "Jaguar",
    "Jet Set",
    "Wasteland Rebel",
    "The Empress"
};

const char* m4a4[] =
{
    "Asiimov",
    "Howl",
    "Dragon King",
    "Poseidon",
    "Daybreak",
    "Royal Paladin",
    "The BattleStar",
    "Desolate Space",
    "Buzz Kill",
	"Noe-Noir"
};

const char* aug[] =
{
    "Bengal Tiger",
    "Hot Rod",
    "Chameleon",
    "Akihabara Accept"
};

const char* famas[] =
{
    "Djinn",
    "Styx",
    "Neural Net",
    "Survivor"
};

const char* awp[] =
{
    "Asiimov",//19
    "Dragon Lore", //1
    "Fever Dream", //0
    "Medusa",//10
    "Hyper Beast",//11
    "BOOM", //0
    "Lightning Strike",//3
    "Pink DDPAT", //2
    "Corticera",//4
    "Redline",//5
    "Man-o'-war",//6
    "Graphite",//7
    "Electric Hive",//18,
	"Mortis"
};

const char* ssg08[] =
{
    "Lichen Dashed",
    "Dark Water",
    "Blue Spruce",
    "Sand Dune",
    "Palm",
    "Mayan Dreams",
    "Blood in the Water",
    "Tropical Storm",
    "Acid Fade",
    "Slashed",
    "Detour",
    "Abyss",
    "Big Iron",
    "Necropos",
    "Ghost Crusader",
    "Dragonfire"
};

const char* scar20[] =
{
    "Blueprint",
    "Cyrex",
    "Emerald",
    "Green Marine",
    "Outbreak",
    "Bloodsport",
    "Jungle Slipstream"
};

const char* p90[] =
{
    "Death by Kitty",
    "Fallout Warning",
    "Scorched",
    "Emerald Dragon",
    "Teardown",
    "Blind Spot",
    "Trigon",
    "Desert Warfare",
    "Module",
    "Asiimov",
    "Elite Build",
    "Shapewood",
    "Shallow Grave"
};

const char* ump45[] =
{
    "Blaze",
    "Minotaur's Labyrinth",
    "Riot",
    "Primal Saber"
};

const char* glock[] =
{
    "Fade",
    "Dragon Tattoo",
    "Twilight Galaxy",
    "Wasteland Rebel",
    "Water Elemental",
    "Off World"
};

const char* usp[] =
{
    "Neo-Noir",
    "Cyrex",
    "Orion",
    "Kill Confirmed",
    "Overgrowth",
    "Caiman",
    "Serum",
    "Guardian",
    "Road Rash"
};

const char* deagle[] =
{
    "Blaze",
    "Kumicho Dragon",
    "Oxide Blaze"
};

const char* tec9[] =
{
    "Nuclear Threat",
    "Red Quartz",
    "Blue Titanium",
    "Titanium Bit",
    "Sandstorm",
    "Isaac",
    "Toxic",
    "Re-Entry",
    "Fuel Injector"
};

const char* p2000[] =
{
    "Handgun",
    "Fade",
    "Corticera",
    "Ocean Foam",
    "Fire Elemental",
    "Asterion",
    "Pathfinder"
};

const char* bhopstyles[] =
{
	"Off",
	"Legit",
	"Auto"
};

const char* p250[] =
{
    "Whiteout",
    "Crimson Kimono",
    "Mint Kimono",
    "Wing Shot",
    "Asiimov",
    "See Ya Later"
};
const char* spammers[] =
{
    "Off",
    "PerfectHook",
    "AimTux",
    "EzFrags"
};