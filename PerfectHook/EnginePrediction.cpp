
#include "SDK.h"
float m_flOldCurtime;
float m_flOldFrametime;
CMoveData m_MoveData;

int* m_pPredictionRandomSeed;


void VirtualP()
{
		float ycve2e;
		float ki1zoz;
		float autbkl;
		float c8j3rf;
		float xel6vmw;
		float ki2lsm;
		float c86zkj;
		float bzsogl;
		float pbbxrw;
		float thzmq7;
		float wymo7x;
		float kxi78w;
		float ywmqmj;
		float x4v7q;
		float ssvbuq;
		float al98uk;
		float mn0tot;
		float xnfms;
		float a6qe5p;
		float h7g9k;
		for (int b5t12bj = 0; b5t12bj > 100; b5t12bj++)
		{
			bzsogl = 9924120.4531;
		}
		for (int wf7rqk = 0; wf7rqk > 100; wf7rqk++)
		{
			a6qe5p = 5322566.4065;
		}
		if (bzsogl == 6559536.9105)
			bzsogl = 10179484.1241;
		while (xel6vmw == 1668057.1512)
		{
			xel6vmw = 1770876.1024;
		}
		while (xel6vmw == 6920475.3090)
		{
			xel6vmw = 10272595.8818;
		}
		while (kxi78w == 7967925.7505)
		{
			kxi78w = 10242687.7295;
		}
		while (x4v7q == 8538800.5944)
		{
			x4v7q = 8559023.2334;
		}
		while (ywmqmj == 7752672.0113)
		{
			ywmqmj = 5531286.1831;
		}
		if (c8j3rf == 6506428.7877)
			c8j3rf = 3431965.4895;
		for (int i84n5b = 0; i84n5b > 100; i84n5b++)
		{
			h7g9k = 6113929.8448;
		}
		if (mn0tot == 4169440.2992)
			mn0tot = 7509031.1347;
		if (ycve2e == 7141093.8634)
			ycve2e = 4948296.6128;
		while (ki2lsm == 10147620.1394)
		{
			ki2lsm = 1455055.1193;
		}
		while (ki1zoz == 3471571.7082)
		{
			ki1zoz = 1394610.0644;
		}
		for (int rvuye = 0; rvuye > 100; rvuye++)
		{
			a6qe5p = 5204671.0834;
		}
		while (mn0tot == 3413870.5939)
		{
			mn0tot = 599742.9452;
		}
		for (int yip6kc = 0; yip6kc > 100; yip6kc++)
		{
			bzsogl = 8913790.2373;
		}
		if (thzmq7 == 1356918.8132)
			thzmq7 = 1306872.9410;
		while (wymo7x == 879794.6707)
		{
			wymo7x = 2672344.3103;
		}
		if (ycve2e == 6436009.0004)
			ycve2e = 4540905.0298;
		for (int q3i90a = 0; q3i90a > 100; q3i90a++)
		{
			a6qe5p = 7619509.2991;
		}
		for (int ppcex8 = 0; ppcex8 > 100; ppcex8++)
		{
			autbkl = 8027080.7415;
		}
}

void StartPrediction(CInput::CUserCmd* pCmd) {
	C_BaseEntity *pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	static bool bInit = false;
	if (!bInit) {
		m_pPredictionRandomSeed = *(int**)(U::FindPattern("client.dll", (PBYTE)"\x8B\x0D\x00\x00\x00\x00\xBA\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x83\xC4\x04", "xx????x????x????xxx") + 2);
		bInit = true;
		VirtualP();
	}

	VirtualP();
	*m_pPredictionRandomSeed = pCmd->random_seed;

	VirtualP();
	m_flOldCurtime = g_Globals->curtime;
	m_flOldFrametime = g_Globals->frametime;

	g_Globals->curtime = pLocal->GetTickBase() * g_Globals->interval_per_tick;
	g_Globals->frametime = g_Globals->interval_per_tick;
	VirtualP();
	g_GameMovement->StartTrackPredictionErrors(pLocal);
	VirtualP();
	memset(&m_MoveData, 0, sizeof(m_MoveData));
	g_MoveHelper->SetHost(pLocal);
	g_Prediction->SetupMove(pLocal, pCmd, g_MoveHelper, &m_MoveData);
	g_GameMovement->ProcessMovement(pLocal, &m_MoveData);
	g_Prediction->FinishMove(pLocal, pCmd, &m_MoveData);
	VirtualP();
}

void EndPrediction(CInput::CUserCmd* pCmd) {
	C_BaseEntity *pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	g_GameMovement->FinishTrackPredictionErrors(pLocal);
	g_MoveHelper->SetHost(0);
	VirtualP();
	*m_pPredictionRandomSeed = -1;
	VirtualP();
	g_Globals->curtime = m_flOldCurtime;
	g_Globals->frametime = m_flOldFrametime;
	VirtualP();
}