#pragma once
#include "SkinChanger.h"
#include "Resolver.h"
#include "HookIncludes.h"
#include "GloveChanger.h"
#include "LagComp.h"
//#include "CorrectionAA.h"
typedef void(__stdcall *fsn_t)(ClientFrameStage_t);

QAngle aim_punch_old;
QAngle view_punch_old;

QAngle* aim_punch = nullptr;
QAngle* view_punch = nullptr;

void  __stdcall hkFrameStageNotify(ClientFrameStage_t curStage)
{
    static auto ofunc = hooks::client.get_original<fsn_t>(36);
    if (g_Engine->IsConnected() && g_Engine->IsInGame())
    {
        if (curStage == FRAME_RENDER_START)
        {
            auto pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
            auto dwDeadFlag = NetVarManager->GetOffset("DT_CSPlayer", "deadflag"); // int


			if (pLocal && pLocal->IsAlive() && g_Options.Visuals.NoVisualRecoil)
			{
				aim_punch =
					(QAngle*)((DWORD)pLocal + offsetz.DT_BasePlayer.m_aimPunchAngle);

				view_punch =
					(QAngle*)((DWORD)pLocal + offsetz.DT_BasePlayer.m_viewPunchAngle);

				aim_punch_old = *aim_punch;
				view_punch_old = *view_punch;

				*aim_punch = QAngle(0, 0, 0);
				*view_punch = QAngle(0, 0, 0);
			}

            if (pLocal)
            {
                if (pLocal->IsAlive() && g_Input->m_fCameraInThirdPerson)
                {
                    *reinterpret_cast<Vector*>(reinterpret_cast<DWORD>(pLocal) + dwDeadFlag + 4) = qLastTickAngles;
                }
            }
        }
    }
	ofunc(curStage);

	if (aim_punch && view_punch && g_Options.Visuals.NoVisualRecoil)
	{
		*aim_punch = aim_punch_old;
		*view_punch = view_punch_old;
	}

	if (curStage == FRAME_NET_UPDATE_POSTDATAUPDATE_START) {

		//if (g_Options.Ragebot.Resolver2)
			//Resolver->AntiAimResolver();
        MemeResolver();
	//	BruteResolver();
		if (GetAsyncKeyState(g_Options.Ragebot.LbyFix)) LbyCorrect();
		if (g_Options.Ragebot.AdvancedCorrection) AdvancedCorrectionpseudo();
        if (g_Options.Skinchanger.Enabled)
        {
            GloveChanger();
            SkinChanger();

        }
        backtracking->Update(g_Globals->tickcount);
	}


}
