#include "HookIncludes.h"
#include "Globals.h"

typedef void(__thiscall *SceneEnd_t)(void *pEcx);

#define STUDIO_RENDER					0x1

float NormalColor[3];
float EColor[3];
float TColor[3];
float IColor1[3];
float IColor2[3];

void __fastcall Hooked_SceneEnd(void *pEcx, void *pEdx) {

	static auto ofunc = hooks::renderview.get_original<SceneEnd_t>(9);

	// Chamsy
	if (g_Options.Visuals.SceneChams == 0) {
		//nic rofl
	}
	else if (g_Options.Visuals.SceneChams == 1) {
		for (int i = 1; i < g_Globals->maxClients; ++i) {
			auto ent = g_EntityList->GetClientEntity(i);
			C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

			if (ent && ent->IsAlive()) {
				static IMaterial* notignorez = CreateMaterial("VertexLitGeneric", "vgui/white_additive", false, true, true, true, true);
				static IMaterial* ignorez = CreateMaterial("VertexLitGeneric", "vgui/white_additive", true, true, true, true, true);

				if (notignorez && ignorez) {

					EColor[0] = (g_Options.Visuals.ChamsT[0]);
					EColor[1] = (g_Options.Visuals.ChamsT[1]);
					EColor[2] = (g_Options.Visuals.ChamsT[2]);

					TColor[0] = (g_Options.Visuals.ChamsCT[0]);
					TColor[1] = (g_Options.Visuals.ChamsCT[1]);
					TColor[2] = (g_Options.Visuals.ChamsCT[2]);

					IColor1[0] = (g_Options.Visuals.XQZT[0]);
					IColor1[1] = (g_Options.Visuals.XQZT[1]);
					IColor1[2] = (g_Options.Visuals.XQZT[2]);

					IColor2[0] = (g_Options.Visuals.XQZCT[0]);
					IColor2[1] = (g_Options.Visuals.XQZCT[1]);
					IColor2[2] = (g_Options.Visuals.XQZCT[2]);

					float alpha = 1.f;

					g_RenderView->SetBlend(alpha);
					if (ent->GetTeamNum() != pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(IColor1);
					else if (ent->GetTeamNum() == pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(IColor2);
					g_ModelRender->ForcedMaterialOverride(ignorez);

					ent->draw_model(STUDIO_RENDER, 255);

					ofunc(pEcx);
					float alpha1 = 1.f;
					g_RenderView->SetBlend(alpha1);
					if (ent->GetTeamNum() != pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(EColor);
					else if (ent->GetTeamNum() == pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(TColor);
					g_ModelRender->ForcedMaterialOverride(notignorez);
					ent->draw_model(STUDIO_RENDER, 255);


					g_ModelRender->ForcedMaterialOverride(nullptr);
				}
			}
		}
	}
	else if (g_Options.Visuals.SceneChams == 2) {
		for (int i = 1; i < g_Globals->maxClients; ++i) {
			auto ent = g_EntityList->GetClientEntity(i);
			C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

			if (ent && ent->IsAlive()) {
				static IMaterial* mat = CreateMaterial("VertexLitGeneric", "vgui/white_additive", false, true, true, true, true);

				if (mat) {

					EColor[0] = (g_Options.Visuals.ChamsT[0]);
					EColor[1] = (g_Options.Visuals.ChamsT[1]);
					EColor[2] = (g_Options.Visuals.ChamsT[2]);

					TColor[0] = (g_Options.Visuals.ChamsCT[0]);
					TColor[1] = (g_Options.Visuals.ChamsCT[1]);
					TColor[2] = (g_Options.Visuals.ChamsCT[2]);

					float alpha = 1.f;

					if (ent->GetTeamNum() != pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(EColor);
					else if (ent->GetTeamNum() == pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(TColor);
					g_RenderView->SetBlend(alpha);
					g_ModelRender->ForcedMaterialOverride(nullptr);
					ent->draw_model(STUDIO_RENDER, 255);
					g_ModelRender->ForcedMaterialOverride(nullptr);
				}
			}
		}
	}
	else if (g_Options.Visuals.SceneChams == 3) {
		for (int i = 1; i < g_Globals->maxClients; ++i) {
			auto ent = g_EntityList->GetClientEntity(i);
			C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

			if (ent && ent->IsAlive()) {
				static IMaterial* notignorez = CreateMaterial2(false, true, false);
				static IMaterial* ignorez = CreateMaterial2(true, true, false);

				if (notignorez && ignorez) {

					EColor[0] = (g_Options.Visuals.ChamsT[0]);
					EColor[1] = (g_Options.Visuals.ChamsT[1]);
					EColor[2] = (g_Options.Visuals.ChamsT[2]);

					TColor[0] = (g_Options.Visuals.ChamsCT[0]);
					TColor[1] = (g_Options.Visuals.ChamsCT[1]);
					TColor[2] = (g_Options.Visuals.ChamsCT[2]);

					IColor1[0] = (g_Options.Visuals.XQZT[0]);
					IColor1[1] = (g_Options.Visuals.XQZT[1]);
					IColor1[2] = (g_Options.Visuals.XQZT[2]);

					IColor2[0] = (g_Options.Visuals.XQZCT[0]);
					IColor2[1] = (g_Options.Visuals.XQZCT[1]);
					IColor2[2] = (g_Options.Visuals.XQZCT[2]);

					float alpha = 1.f;

					g_RenderView->SetBlend(alpha);
					if (ent->GetTeamNum() != pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(IColor1);
					else if (ent->GetTeamNum() == pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(IColor2);
					g_ModelRender->ForcedMaterialOverride(ignorez);

					ent->draw_model(STUDIO_RENDER, 255);

					ofunc(pEcx);
					float alpha1 = 1.f;
					g_RenderView->SetBlend(alpha1);
					if (ent->GetTeamNum() != pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(EColor);
					else if (ent->GetTeamNum() == pLocal->GetTeamNum())
						g_RenderView->SetColorModulation(TColor);
					g_ModelRender->ForcedMaterialOverride(notignorez);
					ent->draw_model(STUDIO_RENDER, 255);


					g_ModelRender->ForcedMaterialOverride(nullptr);
				}
			}
		}
	}

	// Ghost Chamsy
	if (g_Options.Visuals.GhostChams)
	{
		C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
		if (pLocal)
		{
			static  IMaterial* ghost_mat = CreateMaterial("VertexLitGeneric", "vgui/white_additive", false, true, true, true, true);
			if (ghost_mat)
			{
				Vector OrigAng;
				OrigAng = *pLocal->GetEyeAngles();
				pLocal->SetAngle2(Vector(0, Global::FakeAngle, 0));

				bool LbyColor = false;
				float NormalColor[3] = { 1, 1, 1 };
				float lbyUpdateColor[3] = { 0, 1, 0 };
				g_RenderView->SetColorModulation(LbyColor ? lbyUpdateColor : NormalColor);
				g_RenderView->SetBlend(0.3f);
				g_ModelRender->ForcedMaterialOverride(ghost_mat);
				pLocal->draw_model(STUDIO_RENDER, 200);
				g_ModelRender->ForcedMaterialOverride(nullptr);
				pLocal->SetAngle2(OrigAng);
			}
		}
	}
	ofunc(pEcx); // koniec sceneend
}
