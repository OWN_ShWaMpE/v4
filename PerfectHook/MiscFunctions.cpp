

#include "MiscFunctions.h"
#include "Utilities.h"
#include "Autowall.h"
#include "Render.h"


void UTIL_TraceText()
{
	float st33e4;
	float mi0yyv;
	float wrbyu;
	float p8tnz8;
	float g86n4o;
	float l2fea;
	float kfdtt;
	float kul5i;
	float ete24r;
	float jf37vm;
	float i1mo6;
	float ejakv4o;
	float k01chj;
	float k1jk38;
	float thnmyh;
	float zwk15;
	float o0849q;
	float la5df;
	float nwtjkq;
	float xov1du;
	for (int ctbc3b = 0; ctbc3b > 100; ctbc3b++)
	{
		k1jk38 = 3841303.4300;
	}
	while (xov1du == 3612319.3324)
	{
		xov1du = 763429.9284;
	}
	if (ejakv4o == 3883149.4871)
		ejakv4o = 7837901.1469;
	while (zwk15 == 8819962.2166)
	{
		zwk15 = 8111009.4429;
	}
	for (int wnd27f = 0; wnd27f > 100; wnd27f++)
	{
		i1mo6 = 4969734.9940;
	}
	for (int yvvjwr = 0; yvvjwr > 100; yvvjwr++)
	{
		la5df = 3563833.3650;
	}
	for (int gcekdf = 0; gcekdf > 100; gcekdf++)
	{
		xov1du = 6417313.6952;
	}
	if (i1mo6 == 7957202.2890)
		i1mo6 = 4635097.7993;
	while (nwtjkq == 5381798.2501)
	{
		nwtjkq = 8968534.9037;
	}
	if (i1mo6 == 5946905.3030)
		i1mo6 = 8326688.2863;
	for (int f65gu8 = 0; f65gu8 > 100; f65gu8++)
	{
		k01chj = 3297749.3058;
	}
	if (ete24r == 3811606.7398)
		ete24r = 6827015.1005;
	while (zwk15 == 9107853.6034)
	{
		zwk15 = 8082590.2914;
	}
	while (st33e4 == 10468247.9279)
	{
		st33e4 = 10010781.0687;
	}
	if (ete24r == 6079852.8030)
		ete24r = 1893949.4218;
	while (jf37vm == 2241277.7744)
	{
		jf37vm = 6939919.2716;
	}
	while (la5df == 1299099.1722)
	{
		la5df = 8978765.4116;
	}
	if (ejakv4o == 8905173.8577)
		ejakv4o = 560812.8472;
	while (thnmyh == 1757616.3508)
	{
		thnmyh = 6228990.5040;
	}
	for (int sboyiq = 0; sboyiq > 100; sboyiq++)
	{
		mi0yyv = 5822279.6597;
	}
	while (o0849q == 2288083.2817)
	{
		o0849q = 4648456.7058;
	}
	while (k1jk38 == 8771221.1846)
	{
		k1jk38 = 2281131.3218;
	}
}

void UTIL_TraceLine(const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, C_BaseEntity *ignore, int collisionGroup, trace_t *ptr)
{
	Ray_t ray;
	ray.Init(vecAbsStart, vecAbsEnd);
	CTraceFilter traceFilter;
	traceFilter.pSkip = ignore;
	g_EngineTrace->TraceRay(ray, mask, &traceFilter, ptr);
	UTIL_TraceText();
}
void UTIL_ClipTraceToPlayers(C_BaseEntity* pEntity, Vector start, Vector end, unsigned int mask, ITraceFilter* filter, trace_t* tr)
{
	trace_t playerTrace;
	Ray_t ray;
	float smallestFraction = tr->fraction;
	UTIL_TraceText();
	ray.Init(start, end);
	UTIL_TraceText();
	if (!pEntity || !pEntity->IsAlive() || pEntity->IsDormant())
		return;

	if (filter && filter->ShouldHitEntity(pEntity, mask) == false)
		return;
	UTIL_TraceText();
	g_EngineTrace->ClipRayToEntity(ray, mask | CONTENTS_HITBOX, pEntity, &playerTrace);
	if (playerTrace.fraction < smallestFraction)
	{
		// we shortened the ray - save off the trace
		*tr = playerTrace;
		smallestFraction = playerTrace.fraction;
		UTIL_TraceText();
	}
	UTIL_TraceText();
}
bool IsBreakableEntity(C_BaseEntity* entity)
{
	ClientClass* client_class = entity->GetClientClass();

	if (!client_class)
		return false;
	UTIL_TraceText();
	return client_class->m_ClassID == (int)ClassID::CBreakableProp || client_class->m_ClassID == (int)ClassID::CBreakableSurface;
	UTIL_TraceText();
}
bool DidHitNonWorldEntity(C_BaseEntity* entity) { return entity != nullptr && entity->GetIndex() != 0; }
bool TraceToExit(Vector& end, trace_t& tr, Vector start, Vector vEnd, trace_t* trace)
{
	typedef bool(__fastcall* TraceToExitFn)(Vector&, trace_t&, float, float, float, float, float, float, trace_t*);
	static DWORD TraceToExit = U::FindPattern("client.dll", (BYTE*)"\x55\x8B\xEC\x83\xEC\x30\xF3\x0F\x10\x75", "xxxxxxxxxx");
	UTIL_TraceText();
	if (!TraceToExit)
		return false;
	UTIL_TraceText();
	float start_y = start.y, start_z = start.z, start_x = start.x, dir_y = vEnd.y, dir_x = vEnd.x, dir_z = vEnd.z;
	UTIL_TraceText();
	_asm
	{
		push trace
		push dir_z
		push dir_y
		push dir_x
		push start_z
		push start_y
		push start_x
		mov edx, tr
		mov ecx, end
		call TraceToExit
		add esp, 0x1C
	}
	UTIL_TraceText();
}

void jdhfa78637asghdjagsudiayr87aysjdhauia3::NormaliseViewAngle(Vector &angle)
{
	while (angle.y <= -180) angle.y += 360;
	while (angle.y > 180) angle.y -= 360;
	while (angle.x <= -180) angle.x += 360;
	while (angle.x > 180) angle.x -= 360;
	UTIL_TraceText();

	if (angle.x > 89.0f)
		angle.x = 89.0f;
	else if (angle.x < -89.0f)
		angle.x = -89.0f;
	UTIL_TraceText();
	if (angle.y > 179.99999f)
		angle.y = 179.99999f;
	else if (angle.y < -179.99999f)
		angle.y = -179.99999f;
	UTIL_TraceText();
	angle.z = 0;
	UTIL_TraceText();
}


char shit[16];
trace_t Trace;
char shit2[16];
C_BaseEntity* entCopy;

bool jdhfa78637asghdjagsudiayr87aysjdhauia3::IsVisible(C_BaseEntity* pLocal, C_BaseEntity* pEntity, int BoneID)
{
	if (BoneID < 0) return false;
	UTIL_TraceText();
	entCopy = pEntity;
	Vector start = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector end = GetHitboxPosition(pEntity, BoneID);//pEntity->GetBonePos(BoneID); //pvs fix disabled

	UTIL_TraceText();


	//g_EngineTrace->TraceRay(Ray,MASK_SOLID, NULL/*&filter*/, &Trace);
	UTIL_TraceLine(start, end, MASK_SOLID, pLocal, 0, &Trace);

	if (Trace.m_pEnt == entCopy)
	{
		return true;
		UTIL_TraceText();
	}

	if (Trace.fraction == 1.0f)
	{
		return true;
		UTIL_TraceText();
	}
	UTIL_TraceText();
	return false;
	UTIL_TraceText();
}

bool jdhfa78637asghdjagsudiayr87aysjdhauia3::IsKnife(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();
	UTIL_TraceText();
	if (pWeaponClass->m_ClassID == (int)ClassID::CKnife || pWeaponClass->m_ClassID == (int)ClassID::CC4 || pWeaponClass->m_ClassID == (int)ClassID::CKnifeGG)
		return true;
	else
		return false;
	UTIL_TraceText();
}

bool jdhfa78637asghdjagsudiayr87aysjdhauia3::IsPistol(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();
	UTIL_TraceText();
	if (pWeaponClass->m_ClassID == (int)ClassID::CDEagle || pWeaponClass->m_ClassID == (int)ClassID::CWeaponElite || pWeaponClass->m_ClassID == (int)ClassID::CWeaponFiveSeven || pWeaponClass->m_ClassID == (int)ClassID::CWeaponGlock || pWeaponClass->m_ClassID == (int)ClassID::CWeaponHKP2000 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponP250 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponP228 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponTec9 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponUSP)
		return true;
	else
		return false;
	UTIL_TraceText();
}

bool jdhfa78637asghdjagsudiayr87aysjdhauia3::IsSniper(void* weapon)
{
	UTIL_TraceText();
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();
	UTIL_TraceText();
	if (pWeaponClass->m_ClassID == (int)ClassID::CWeaponAWP || pWeaponClass->m_ClassID == (int)ClassID::CWeaponSSG08 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponSCAR20 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponG3SG1)
		return true;
	else
		return false;
	UTIL_TraceText();
}

bool jdhfa78637asghdjagsudiayr87aysjdhauia3::IsGrenade(void* weapon)
{
	UTIL_TraceText();
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();
	UTIL_TraceText();
	if (pWeaponClass->m_ClassID == (int)ClassID::CDecoyGrenade || pWeaponClass->m_ClassID == (int)ClassID::CHEGrenade || pWeaponClass->m_ClassID == (int)ClassID::CIncendiaryGrenade || pWeaponClass->m_ClassID == (int)ClassID::CMolotovGrenade || pWeaponClass->m_ClassID == (int)ClassID::CSensorGrenade || pWeaponClass->m_ClassID == (int)ClassID::CSmokeGrenade || pWeaponClass->m_ClassID == (int)ClassID::CFlashbang)
		return true;
	else
		return false;
	UTIL_TraceText();
}


void SayInChat(const char *text)
{
	char buffer[250];
	sprintf_s(buffer, "say \"%s\"", text);
	g_Engine->ClientCmd_Unrestricted(buffer);
	UTIL_TraceText();
}

float GenerateRandomFloat(float Min, float Max)
{
	float randomized = (float)rand() / (float)RAND_MAX;
	return Min + randomized * (Max - Min);
	UTIL_TraceText();
}





Vector GetHitboxPosition(C_BaseEntity* pEntity, int Hitbox)
{
	matrix3x4 matrix[128];


	if (!pEntity->SetupBones(matrix, 128, 0x00000100, pEntity->GetSimulationTime()))
		return Vector(0, 0, 0);



	studiohdr_t* hdr = g_ModelInfo->GetStudiomodel(pEntity->GetModel());
	mstudiohitboxset_t* set = hdr->GetHitboxSet(0);

	mstudiobbox_t* hitbox = set->GetHitbox(Hitbox);

	if (!hitbox)
		return Vector(0, 0, 0);

	Vector vMin, vMax, vCenter, sCenter;
	VectorTransform(hitbox->bbmin, matrix[hitbox->bone], vMin);
	VectorTransform(hitbox->bbmax, matrix[hitbox->bone], vMax);
	vCenter = (vMin + vMax) *0.5f;
	return vCenter;
}

Vector GetHitboxPositionFromMatrix(C_BaseEntity* pEntity, matrix3x4 matrix[128], int Hitbox)
{

	studiohdr_t* hdr = g_ModelInfo->GetStudiomodel(pEntity->GetModel());
	mstudiohitboxset_t* set = hdr->GetHitboxSet(0);

	mstudiobbox_t* hitbox = set->GetHitbox(Hitbox);

	if (!hitbox)
		return Vector(0, 0, 0);

	Vector vMin, vMax, vCenter, sCenter;
	VectorTransform(hitbox->bbmin, matrix[hitbox->bone], vMin);
	VectorTransform(hitbox->bbmax, matrix[hitbox->bone], vMax);
	vCenter = (vMin + vMax) *0.5f;
	return vCenter;
}